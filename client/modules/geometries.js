import { BoxGeometry, SphereGeometry, TorusKnotGeometry } from 'three';

export const torus = new TorusKnotGeometry(0.075, 0.02, 128, 24, 4, 2);
export const box = new BoxGeometry(0.15, 0.15, 0.15);
export const sphere = new SphereGeometry(0.2, 60, 40);
