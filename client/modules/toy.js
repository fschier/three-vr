import { Object3D, CubeCamera, Mesh, Clock, WebGLCubeRenderTarget, RGBFormat, LinearMipmapLinearFilter } from 'three';

import { metal1, metal2, normal, dark, toon, wire } from './materials';
import { torus, box, sphere } from './geometries';

import autoBind from 'auto-bind';

export default class Toy {
  constructor(renderer, scene) {
    this.renderer = renderer;
    this.scene = scene;
    autoBind(this);
    this.clock = new Clock();
    this.oscillator = 0;

    this.object = new Object3D();
    this.object.name = 'toy';

    this.toy = new Mesh(sphere, metal1);
    this.toy.name = 'toyMesh';
    this.toy.position.set(0.3, 1.25, -0.2);
    this.toy.castShadow = true;
    this.toy.receiveShadow = !this.toy.material.userData.toon;
    this.object.add(this.toy);

    this.cubeRenderTarget = new WebGLCubeRenderTarget(256, { format: RGBFormat, generateMipmaps: true, minFilter: LinearMipmapLinearFilter });
    this.cubeCamera = new CubeCamera(0.1, 20, this.cubeRenderTarget);

    if (this.toy.material.userData.mettalic) this.toy.material.envMap = this.cubeRenderTarget.texture;
    this.cubeCamera.position.copy(this.toy.position);
    this.object.add(this.cubeCamera);
  }

  update() {
    this.toy.rotation.y += 0.005;
    this.toy.rotation.z += 0.005;
    this.toy.rotation.x += 0.001;
    this.oscillator += this.clock.getDelta();
    this.toy.position.y = (Math.sin(this.oscillator) * 0.1) + 1.4;
    this.cubeCamera.position.y = this.toy.position.y;
    this.cubeCamera.update(this.object.parent.renderer, this.object.parent.parent);
  }
}
